import Phaser from 'phaser';

import Game from './Game';
import PreGame from './PreGame';
import PostGame from './PostGame';
import LeaderBoard from './LeaderBoard';
import PostLeaderBoard from './PostLeaderBoard';

let CONFIG;

const body = document.querySelector('body');
body.style.backgroundImage = `url(./assets/bg2.png)`;
body.style.backgroundRepeat = 'no-repeat';
body.style.backgroundSize = 'cover';
body.style.backgroundPosition = 'bottom';

CONFIG = {
  type: Phaser.AUTO,
  backgroundColor: '#000000',
  width: 740,
  height: 960,
  parent: 'phaser-example',
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  dom: {
    createContainer: true,
  },
  scene: [PreGame, LeaderBoard, Game, PostGame, PostLeaderBoard],
  physics: {
    default: 'matter',
    // matter: {
    //   debug: true,
    // },
  },
};

let game = new Phaser.Game(CONFIG);
